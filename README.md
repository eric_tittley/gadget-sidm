Gadget modified to implement Self-interacting dark matter

Contributors:
 * Volker Springle and all those who contributed to Gadget-2
 * Eric Tittley

Project page:
 https://bitbucket.org/eric_tittley/gadget-sidm

Download
 git clone git@bitbucket.org:eric_tittley/gadget-sidm.git